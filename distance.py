import math
import random

DISTANCE = 'distance'
LEARN_INDEX = 'learn_index'


def count_distance(learning_data, testing_data, settings):
    learning_shape = learning_data.shape
    distances_per_test_row = [[] for i in range(testing_data.shape[0])]  ## list of list of distances per testing row

    test_row_id = 0
    for test in testing_data:
        i = 0
        i = i + 1
        print('test_row ' + str(test_row_id) + ' -----------------------------------------')
        for learn_index in range(learning_shape[0]):
            distances_per_test_row[test_row_id].append(
                {LEARN_INDEX: i, DISTANCE: run_distance_counter(learning_data[learn_index], test, settings)})
        test_row_id = test_row_id + 1

    for d in distances_per_test_row:
        d.sort(key=lambda dis: dis[DISTANCE])

    return distances_per_test_row


def run_distance_counter(a, b, settings):
    if settings.metric == 'euclid':
        return euclid_distance(a, b, settings.d)
    else:
        return manhattan_distance(a, b, settings.d)


def euclid_distance(a, b, decision_col_index):
    distance = 0.0
    for columnNumber in range(len(a)):
        if columnNumber == decision_col_index:
            continue
        diff = float(a[columnNumber]) - float(b[columnNumber])
        distance = distance + diff * diff
    distance = math.sqrt(distance)
    print('distance: ' + str(distance))
    return distance


def manhattan_distance(a, b, decision_col_index):
    distance = 0.0
    for columnNumber in range(len(a)):
        if columnNumber == decision_col_index:
            continue
        diff = float(a[columnNumber]) - float(b[columnNumber])
        distance = distance + abs(diff)
    print('distance: ' + str(distance))
    return distance


def find_main_decision(decisions):
    max_count = 0
    possible_result = []
    for d in decisions:
        if decisions[d] > max_count:
            max_count = decisions[d]
    for d in decisions:
        if decisions[d] == max_count:
            possible_result.append(d)
    random_value = random.randint(0, len(possible_result) - 1)
    return possible_result[random_value]


def count_decision(nearest, learning_set, decision_col):
    final_decision = []
    decision = {}
    for near in nearest:
        for n in near:
            decision[str(learning_set[n[LEARN_INDEX]][decision_col])] = 0
        for n in near:
            var = learning_set[n[LEARN_INDEX]]
            real_decision = str(var[decision_col])
            decision[real_decision] = decision[real_decision] + 1
        final_decision.append(find_main_decision(decision))

    return final_decision
