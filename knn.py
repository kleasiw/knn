import numpy as np

import distance as dis

K_CROSS = 10


def set_nearest_neighbor(dist, nearest, test_row_index, k):
    for i in range(k):
        nearest[test_row_index].append(dist[i])


def count_knn(settings, learning_set, testing_set):
    if learning_set.shape[0] < 1 or testing_set.shape[0] < 1:
        raise ValueError('Empty learning or testing data set, probably because percentage split was set as 0 or 1')

    distances_per_test_row = []
    if settings.train == 'cross':
        cross_validation(distances_per_test_row, learning_set, settings, testing_set)
    else:
        counted_distances = dis.count_distance(learning_set, testing_set, settings)
        append_counted_distances(counted_distances, distances_per_test_row)

    nearest_per_test_row = [[] for i in range(testing_set.shape[0])]
    get_nearest_distances(distances_per_test_row, nearest_per_test_row, settings)

    return dis.count_decision(nearest_per_test_row, learning_set, settings.d)


def cross_validation(distances_per_test_row, learning_set, settings, testing_set):
    file_rows_size = learning_set.shape[0]
    rows_per_fragment = int(file_rows_size / K_CROSS)
    for i in range(0, K_CROSS):
        learning_set_part, testing_set_part = prepare_cross_data_sets(i, learning_set, rows_per_fragment, testing_set)
        counted_distances = dis.count_distance(learning_set_part, testing_set_part, settings)
        append_counted_distances(counted_distances, distances_per_test_row)


def prepare_cross_data_sets(i, learning_set, rows_per_fragment, testing_set):
    min_row = rows_per_fragment * i
    max_row = min_row + rows_per_fragment
    testing_set_part = testing_set[range(min_row, max_row)]
    learning_set_part = learning_set[:]
    learning_set_part = np.delete(learning_set_part, [range(min_row, max_row)], 0)
    return learning_set_part, testing_set_part


def append_counted_distances(counted_distances, distances_per_test_row):
    for c in counted_distances:
        distances_per_test_row.append(c)


def get_nearest_distances(distances_per_test_row, nearest_per_test_row, settings):
    test_row_index = 0
    for dist in distances_per_test_row:
        set_nearest_neighbor(dist, nearest_per_test_row, test_row_index, settings.k)
        test_row_index = test_row_index + 1


def count_accuracy(results, full_data, settings):
    index = 0
    positive = 0
    all = len(results)
    if settings.train == 'split':
        start_index = int(settings.percentage_split * full_data.shape[0])
    else:
        start_index = 0
    for r in results:
        s = str(full_data[start_index + index][settings.d])
        if r == s:
            positive = positive + 1
        index = index + 1
    return positive / all * 100
