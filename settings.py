class Settings:
    k = 0
    metric = ''
    d = 0
    file = ''
    train = ''

    def __init__(self, args):
        self.k = args.k
        self.metric = args.m
        self.d = args.d
        self.file = args.file
        self.train = args.t
        self.percentage_split = args.p
