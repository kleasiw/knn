import argparse
import numpy as np
import knn
import settings


def read_args_from_command():
    parser = argparse.ArgumentParser(description='k-NN algorithm')
    parser.add_argument('-k', metavar='', type=int, default=5, help='Number of nearest neighbors. Default: 5')
    parser.add_argument('-m', '-metric', metavar='', type=str, default='euclid',
                        help='Metrics: euclid(default) or manhattan')
    parser.add_argument('-t', '-train', metavar='', type=str, default='train',
                        help='Testing category: train(default), split, cross')
    parser.add_argument('-d', type=int, help='Index of column with decisions', required=True)
    parser.add_argument('-file', type=str, help='File with data', required=True)
    parser.add_argument('-p', type=float, default=0.25, help='Percentage split: (0,1)')
    return parser.parse_args()


def read_file(settings):
    return np.loadtxt(settings.file, delimiter=",", dtype='str')


def prepare_learning_data(full_data, settings):
    if settings.train == 'split':
        rows_value = int(settings.percentage_split * full_data.shape[0])
        new_data = full_data[range(0, rows_value)]
        return new_data
    else:
        return full_data


def prepare_testing_data(full_data, settings):
    if settings.train == 'split':
        rows_value = int(settings.percentage_split * full_data.shape[0])
        new_data = full_data[range(rows_value, full_data.shape[0])]
        return new_data
    else:
        return full_data


def print_results(results):
    print('id \t result')
    index = 0
    for r in results:
        print(str(index) + '\t' + r)
        index = index + 1
    pass


def print_results_with_comparison(results, settings):
    print('id \t result \t real decision')
    if settings.train == 'split':
        start_index = int(settings.percentage_split * file.shape[0])
    else:
        start_index = 0
    for r in results:
        print(str(start_index) + '\t\t' + r + '\t\t\t' + str(file[start_index][settings.d]))
        start_index = start_index + 1
    pass


args = read_args_from_command()
settings = settings.Settings(args)
file = read_file(settings)
learning_set = prepare_learning_data(file, settings)
testing_set = prepare_testing_data(file, settings)

result = knn.count_knn(settings, learning_set, testing_set)
print_results_with_comparison(result, settings)

print('Trafność: ' + str(knn.count_accuracy(result, file, settings)) + '%')
print("Data from file: [" + str(file.shape[0]) + "," + str(file.shape[1]) + "]")
